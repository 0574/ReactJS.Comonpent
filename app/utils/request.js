const REQUEST_HOST = '';

var mockdata;

function refreshMock(){
    mockdata = {
        //Giab mock
        //用户单点登录模拟接口
        '/user/login':
            Mock.mock(
                {
                    'data':"",
                    'success':true
                }
            )
    };
}

/*
 * url 请求地址
 * rtype 请求方式 GET,POST,PUT,DELETE
 * type 请求类型 mock,truth 模拟/真实
 * data 参数
 * callback 回调方法 type:0 成功 1数据异常 2 服务器异常
 */
function query (url,rtype,type,data,callback){
    if (type == "mock"){
        refreshMock();
        var d = mockdata[url];
        callback(d,requestSuccess);
    }else if (type == "truth"){
        $.ajax({
            "url":REQUEST_HOST + url,
            "async": true,
            "cache":false,
            "method": rtype,
            "data":data,
            "dataType": 'json',
            "xhrFields": {
                "withCredentials": true
            },
            timeout : 20000,
            "crossDomain": true,
            success:function(d){
                if (d.success == false){
                    //有数据错误
                    callback(d.error,requestDataError);
                }else {
                    //成功
                    callback(d,requestSuccess);
                }
            },
            error:function(e){
                //服务异常
                callback(e,requestServiceError);
            }
        });
    }
}



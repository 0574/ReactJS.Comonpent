import React, {Component} from 'react';
import {Picker} from  '../CommonReactJS.js';
import {CSS} from './PickerView.css'
export default class PickerView extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.state = {}
    }

    render() {
        return (
            <div>
                <div className="picker_BoxDiv">
                    <div className="pickerViewTabr_box">
                        <div className="left_bar" onClick={this.props.cancel}>取消</div>
                        <div className="right_bar"onClick={this.props.finish} >完成</div>
                    </div>
                    <Picker
                        optionGroups={this.props.optionGroups}
                        valueGroups={this.props.valueGroups}
                        onChange={this.props.onChange}/>
                </div>
            </div>
        )
    }
}


PickerView.propTypes = {
    // 可选择数据
    optionGroups:React.PropTypes.object,
    //默认数据
    valueGroups:React.PropTypes.object,
    //值改变事件
    onChange:React.PropTypes.func,
    //完成
    finish:React.PropTypes.func,
    //取消
    cancel:React.PropTypes.func,

}


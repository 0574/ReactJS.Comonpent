import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slick from "react-slick";
//import classNames from 'classnames';
//import {CSS} from '../../resources/css/IndexContainer.css';

export default class Carousel extends Component{
    constructor(props){
        super(props);
    }

    componentWillMount(){

    }



    render(){
        let props = this.props;
        return (
            <Slick {...props} />
        )
    }
}


Carousel.propTypes = {
    accessibility: PropTypes.bool,	    //Enables tabbing and arrow key navigation	Yes
    className: PropTypes.string,	    //Additional class name for the inner slider div
    arrows:	PropTypes.bool,     	    //Should we show Left and right nav arrows	Yes
    nextArrow: PropTypes.element, 	    //Use this element for the next arrow button Example	Yes
    prevArrow: PropTypes.element,	    //Use this element for the prev arrow button Example	Yes
    autoplay: PropTypes.bool,   	    //Should the scroller auto scroll?	Yes
    autoplaySpeed: PropTypes.number,	//delay between each auto scoll. in ms	Yes
    centerMode:	PropTypes.bool,     	//Should we centre to a single item?	Yes
    centerPadding: PropTypes.number,    //if centerMode is true, please give me the padding
    draggable: PropTypes.bool,	        //Is the gallery scrollable via dragging on desktop?	Yes
    fade: PropTypes.bool,	            //Slides use fade for transition	Yes
    focusOnSelect: PropTypes.bool,	    //Go to slide on click	Yes
    infinite: PropTypes.bool,	        //循环
    initialSlide: PropTypes.number,	    //初始index
    responsive:	PropTypes.array,	    //Array of objects in the form of { breakpoint: int, settings: { ... } } The breakpoint int is the maxWidth so the settings will be applied when resolution is below this value. Breakpoints in the array should be ordered from smallest to greatest. Use 'unslick' in place of the settings object to disable rendering the carousel at that breakpoint. Example: [ { breakpoint: 768, settings: { slidesToShow: 3 } }, { breakpoint: 1024, settings: { slidesToShow: 5 } }, { breakpoint: 100000, settings: 'unslick' } ]	Yes
    rtl: PropTypes.bool,	            //倒序
    speed: PropTypes.number,
    swipe: PropTypes.bool,
    swipeToSlide: PropTypes.bool,	    //Allow users to drag or swipe directly to a slide irrespective of slidesToScroll	Yes
    vertical: PropTypes.bool,	        //Vertical slide mode	Yes
    afterChange: PropTypes.func,	    //callback function called after the current index changes. The new index is accessible in the first parameter of the callback function.	Yes
    beforeChange: PropTypes.func,	    //callback function called before the current index changes. The old and the new indexes are accessible in the first and the second parameters of the callback function respectively.	Yes
    slickGoTo: PropTypes.number	        //go to the specified slide number
    /*react-slick 有但是感觉暂时没啥卵用的props*/
    //adaptiveHeight:	PropTypes.bool,	    //Adjust the slide's height automatically	Yes
    //cssEase: PropTypes.bool,
    //customPaging: PropTypes.func,   	//Custom paging templates. Example	Yes
    //easing:	PropTypes.string,
    //lazyLoad	bool	Loads images or renders components on demands	Yes
    //pauseOnHover	bool	prevents autoplay while hovering	Yes
    //slide	string
    //slidesToShow	int	Number of slides to be visible at a time	Yes
    //slidesToScroll	int	Number of slides to scroll for each navigation item
    //touchMove	bool
    //touchThreshold	int
    //variableWidth	bool
    //useCSS	bool	Enable/Disable CSS Transitions	Yes
    //dots: PropTypes.bool,	            //Should we show the dots at the bottom of the gallery	Yes
    //dotsClass: PropTypes.string,	    //Class applied to the dots if they are enabled	Yes
};
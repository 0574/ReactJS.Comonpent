import React from 'react';
import {Router, Route, Link, IndexRoute, useRouterHistory} from 'react-router';
import App from '../containers/App.jsx';


import Index from '../containers/IndexContainer.jsx';
import DatePickerContainer from '../containers/DatePickerContainer.jsx';
import PickerContainer from '../containers/PickerContainer.jsx';
export default (
    <Route path="/" component={App}>
        <IndexRoute component={Index}/>
        <Route path="/DatePicker" component={DatePickerContainer}/>
        <Route path="/PickerView" component={PickerContainer}/>
    </Route>
);
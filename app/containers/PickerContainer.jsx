import React, {Component} from 'react';
import {CSS} from  '../../resources/css/PickerContainer.css'
import Picker from '../components/PickerView/PickerView.jsx';
import {Toast} from '../components/CommonReactJS';
export default class PickerContainer extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.state = {
            //默认展示数据
            valueGroups: {
                title: 'Mr.',
                firstName: 'Micheal',
                secondName: 'Jordan'
            },
            //可选择数据
            optionGroups: {
                title: ['Mr.', 'Mrs.', 'Ms.', 'Dr.'],
                firstName: ['John', 'Micheal', 'Elizabeth'],
                secondName: ['Lennon', 'Jackson', 'Jordan', 'Legend', 'Taylor']
            },

            displayPicker:false,
        }
    }

    handleClick() {
        this.setState({
            displayPicker:true,
        })
    }

    handleChange(name,value){
        let valueGroups = this.state.valueGroups;
        valueGroups[name] = value;
        this.setState({
            valueGroups:valueGroups,
        });
    };
    handleFinish(){
        let value = this.state.valueGroups.title + "-"
            +this.state.valueGroups.firstName + "-"
            +this.state.valueGroups.secondName;
        Toast.success(value, 1000, 'fa-check',false,()=>{
            this.setState({
                displayPicker:false,
            })
        });
    }

    handleCancel(){
        this.setState({
            displayPicker:false,
        })
    }
    render() {
        const {optionGroups, valueGroups} = this.state;
        let tempArea = "";
        if(this.state.displayPicker === false){
            tempArea = "";
        }else if(this.state.displayPicker === true){
           tempArea =  <Picker
                optionGroups={optionGroups}
                valueGroups={valueGroups}
                onChange={this.handleChange.bind(this)}
                finish={this.handleFinish.bind(this)}
                cancel={this.handleCancel.bind(this)}
            />
        }
        return (
            <div>
                <div className="pickerValueTitle" onClick={this.handleClick.bind(this)}>{valueGroups.title}  - {valueGroups.firstName} - {valueGroups.secondName}</div>
                {tempArea}
            </div>
        )
    }
}
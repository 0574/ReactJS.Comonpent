import React, { Component } from 'react'
import "babel-polyfill";



export default class App extends Component {

    constructor(props){
        super(props);
    }

    componentWillMount(){
        //window.addEventListener("resize", this.onResizeHandle);
    }

    componentWillUnmount(){
        //window.removeEventListener("resize", this.onResizeHandle);
    }


    render() {
        return (
            <div>
                {this.props.children}
            </div>
        )
    }
}

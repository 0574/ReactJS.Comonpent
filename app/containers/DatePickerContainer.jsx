import React, {Component} from 'react';
import {DatePicker} from '../components/CommonReactJS';
import {CSS} from '../../resources/css/DatePickerContainer.css'
import PropTypes from 'prop-types';
export default class DatePickerContainer extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.state = {
            isOpen:false,
            timeValue:"点击选择",
        }

    }

    //确认
    handleSelect(time){
        let year = time.getFullYear();
        let month = time.getMonth() + 1;
        let day = time.getDate();
        let timeValue = year+"-"+month+"-"+day;
        this.setState({
            timeValue:timeValue,
            isOpen:false,
        })
    }

    //取消
    handleCancel(){
        this.setState({
            isOpen:false,
        })
    }

    handleClick(){
        this.setState({
            isOpen:true,
        })
    }

    render() {
        return (
            <div>
                <div className="dateValueTitle" onClick={this.handleClick.bind(this)}>{this.state.timeValue}</div>
                <DatePicker
                    isOpen={this.state.isOpen}
                    onSelect={this.handleSelect.bind(this)}
                    onCancel={this.handleCancel.bind(this)}
                />
            </div>
        )
    }
}


//(属性)         (类型)         (默认值)                       (描述)
// Property	    Type	       Default	                    Description
// isPopup	    Boolean	       true	                        whether as popup add a overlay
// isOpen	    Boolean	       false	                    whether to open datepicker
// theme	    String	       default	                    theme of datepicker, include 'default', 'dark', 'ios', 'android', 'android-dark'
// dateFormat	Array	       ['YYYY', 'M', 'D']	        according to year, month, day, hour, minute, second format specified display text. E.g ['YYYY年', 'MM月', 'DD日']
// showFormat	String	       'YYYY/MM/DD'	                customize the format of the display title
// value	    Date	       new Date()	                date value
// min	        Date	       new Date(1970, 0, 1)	        minimum date
// max	        Date	       new Date(2050, 0, 1)	        maximum date
// confirmText  String	       完成	                        customize the selection time button text
// cancelText	String	       取消	                        customize the cancel button text
// onSelect	    Function	   () => {}	                    the callback function after click button of done, Date object as a parameter
// onCancel	    Function	   () => {}	                    the callback function after click button of cancel
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Toast,Carousel} from '../components/CommonReactJS';
import {CSS} from '../../resources/css/IndexContainer.css';

export default class IndexContainer extends Component{
    constructor(props){
        super(props);

        this.state = {
            //carouselDots:false,
            //carouselDotsClass:"",
            //carouselArrows:false,
            //carouselAutoplay:false,
            //carouselAutoplaySpeed:5000,
            //carouselFade:false,
            //carouselInfinite:false,
            //carouselRtl:false
        };
    }


    componentWillMount(){

    }

    /****************提示框******************/

    //成功
    toastSuccess() {
        let value = this.refs.toastSucess.value;
        if (value.length == 0){
            value = '操作成功';
        }
        Toast.success(value, 1000, 'fa-check',false,()=>{
            //add you method
        });
    }

    //异常
    toastError() {
        let value = this.refs.toastError.value;
        if (value.length == 0){
            value = '操作失败';
        }
        Toast.error(value, 1000, 'fa-times', false, ()=>{
            //add you method
        });
    }

    //警告
    toastWarning() {
        let value = this.refs.toastWarning.value;
        Toast.warning('警告!'+ value, 1000, 'fa-exclamation-triangle', false, ()=>{
            //add you method
        });
    }

    //加载中
    toastLoading() {
        Toast.show('加载中...', 0, 'fa-circle-o-notch fa-spin', false);
        const timer = setTimeout(()=>{
            Toast.hide();
            clearTimeout(timer);
        }, 2000);
    }

    //常规
    toastShow() {
        Toast.show('提示!', 1000, undefined, false, ()=>{
            //add you method
        });
    }


    /*******************轮播********************/

    autoplayClick(e) {
        this.setState({
            carouselAutoplay:true
        });
        if (this.state.carouselAutoplay == true){
            e.target.textContent = "自动播放";
        } else {
            e.target.textContent = "停止播放";
        }



    }


    render(){
        let screenWidth = $(window).width();

        return (
            <div>
                <div className="index_title">提示语</div>
                <div className="index_toast_box">
                    <input ref="toastSucess" className="index_toast_input" placeholder="输入提示语…"/>
                    <div className="index_toast_button" onClick={this.toastSuccess.bind(this)}>成功</div>
                    <div style={{clear:"both"}}></div>
                </div>
                <div className="index_toast_box">
                    <input ref="toastError" className="index_toast_input" placeholder="输入提示语…"/>
                    <div className="index_toast_button" onClick={this.toastError.bind(this)}>失败</div>
                    <div style={{clear:"both"}}></div>
                </div>
                <div className="index_toast_box">
                    <input ref="toastWarning" className="index_toast_input" placeholder="输入提示语…"/>
                    <div className="index_toast_button" onClick={this.toastWarning.bind(this)}>警告</div>
                    <div style={{clear:"both"}}></div>
                </div>
                <div style={{margin:"0.5rem 0 0 5vw",float:"none"}} className="index_toast_button" onClick={this.toastLoading.bind(this)}>加载</div>
                <div style={{margin:"1rem 0 0 5vw",float:"none"}} className="index_toast_button" onClick={this.toastShow.bind(this)}>Normal</div>
                <div className="index_sec_title"></div>
                <div className="index_title">轮播</div>
                <div className="index_sec_title"></div>
                <Carousel
                    className="index_carousel_center"
                    arrows={false}
                    //循环
                    infinite={false}
                    //自动播放
                    autoplay={false}
                    autoplaySpeed={5000}
                    //叠化
                    fade={false}
                    centerMode={true}
                    centerPadding={screenWidth*0.1}
                >
                    <div className="index_carousel_center_item"><div className="index_carousel_center_item_content">1</div></div>
                    <div className="index_carousel_center_item"><div className="index_carousel_center_item_content">2</div></div>
                    <div className="index_carousel_center_item"><div className="index_carousel_center_item_content">3</div></div>
                </Carousel>
                <div className="index_sec_title"></div>
                <Carousel
                    className="index_carousel_full"
                    arrows={false}
                    //循环
                    infinite={true}
                    //自动播放
                    autoplay={true}
                    autoplaySpeed={5000}
                    //叠化
                    fade={false}
                >
                    <div className="index_carousel_full_item">1</div>
                    <div className="index_carousel_full_item">2</div>
                    <div className="index_carousel_full_item">3</div>
                </Carousel>
                <div className="index_sec_title"></div>

            </div>
        )
    }
}